import time
import serial
from tkinter import *
from PIL import ImageTk, Image
import os
import linecache
import glob


#conexão serial

#ser = serial.Serial('/dev/cu.usbmodem1421', baudrate = 9600, timeout = 1)
#ser = serial.Serial('/dev/cu.wchusbserial1420', baudrate = 9600, timeout = 1)
#ser = serial.Serial('/dev/cu.usbmodem1411', baudrate = 9600, timeout = 1)
#arduino nano Wawa
#ser = serial.Serial('/dev/cu.wchusbserial1410', baudrate = 9600, timeout = 1)
#pcb pedaleira
#ser = serial.Serial('/dev/cu.usbserial-A10722EM', baudrate = 9600, timeout = 1)
#hardware interface
ser = serial.Serial('/dev/cu.SLAB_USBtoUART', baudrate = 19200, timeout = 1)
time.sleep(3)


class inicio():
        #criar arquivo ao inicializar e remover arquivo anterior
    def __init__(self):
        root.after(1, removeFiles)
        root.after(10, setNomePresets)

    #setar os botoes de preset um pouco apos inicializar
    #def __init__(self):
        #root.after(100, setNomePresets)


#this piece's not mine, set "background message" in entry
def on_entry_click(event):
    """function that gets called whenever entry is clicked"""
    if presetEntry.get() == 'Insira nome do preset...':
       presetEntry.delete(0, "end") # delete all the text in the entry
       presetEntry.insert(0, '') #Insert blank for user input
       presetEntry.config(fg = 'black')

def on_focusout(event):
    if presetEntry.get() == '':
        presetEntry.insert(0, 'Insira nome do preset...')
        presetEntry.config(fg = 'grey')


#remove arquivos indesejados ao iniciar
def removeFiles():
    if os.path.isfile("listaPresets.txt"):
        file2 = open("listaPresets.txt", "r")
        lines = file2.readlines()
        length = len(lines)
        cleanupFiles = glob.glob("*.txt")
        for x in range(0,length):
            #if not (os.path.isfile("%s.txt" %lines[x].strip())):
            cleanupFiles.remove("%s.txt" %lines[x].strip())
        cleanupFiles.remove("listaPresets.txt")
        if os.path.isfile("infoKnobs.txt"):
            cleanupFiles.remove("infoKnobs.txt")
        for cleanupFile in cleanupFiles:
            os.remove(cleanupFile)
            #print(cleaunpFiles)
        file2.close()
    if os.path.isfile("infoKnobs.txt"):
        os.remove("infoKnobs.txt")
    criaFiles()

#cria arquivo desejado ao inciar
def criaFiles():
    file1 = open("infoKnobs.txt", "w")
    file1.write("0") #escrever 0 para que a msg serial não seja enviada ao iniciar
    file1.close()

#muda imagem dos knobs de acordo com o valor do fader
def mudaImagemKnob():
    file1 = open("infoKnobs.txt", "r") #arquivo para guardar knob escolhido
    qualKnob = file1.readline()
    file1.close()
    esseKnob = globals()['knob%s' %qualKnob]
    #variaveis auxiliares p/ range
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        if qualKnob == "0": #se nenhum knob foi escolhido, sai.
            break
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y < posicao < z: #avalia valor do scale, muda imagem do knob de acordo com o range
            esseKnob.config(image = knobImage)
        y = z + 1

#funções para ver qual knob foi selecionado e destacá-lo (apagando as bordas dos demais)
def qualKnob1(event):
    qualKnob = 1
    event.widget.config(borderwidth = 0.8)
    for x in range(1,12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w") #arquivo para guardar knob escolhido
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)#envia comando de ler knob p/ arduino
    posicaoLida = recebeSerial2() #recebe a posição lida
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #mudaImagemKnob(esseKnob)
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8
    #scale.set(posicaoLida)

def qualKnob2(event):
    qualKnob = 2
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    #scale.set(posicaoLida)
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL

def qualKnob3(event):
    qualKnob = 3
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL

def qualKnob4(event):
    qualKnob = 4
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    root.after(10, None)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL

def qualKnob5(event):
    qualKnob = 5
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    posicaoLida = posicaoLida.split("\n")[:-1][0]
    posicaoLida = float(posicaoLida)/1.8
    scale.set(posicaoLida)#mostra, no fader, posicao atual do kno
    #posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        #posicaoLida = int(posicaoLida)/1.8
        posicaoLida = int(posicaoLida)
        if y <= posicaoLida <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            knob5.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #print(posicaoLida)
    #scale.set(posicaoLida) #VERSAO FINAAAAL

def qualKnob6(event):
    qualKnob = 6
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    #canvas1.after(500, None)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL


def qualKnob7(event):
    qualKnob = 7
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8) #mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL


def qualKnob8(event):
    qualKnob = 8
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    scale.set(float(posicaoLida.split("\n")[:-1][0])/1.8)#mostra, no fader, posicao atual do kno
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1
    #posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    #scale.set(posicaoLida) #VERSAO FINAAAAL

def qualKnob9(event):
    qualKnob = 9
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob) #envia comando de ler knob p/ arduino
    posicaoLida = recebeSerial2() #recebe a posição lida
    scale.set(posicaoLida.split("\n")[:-1][0])#mostra, no fader, posicao atual do kno
    posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    scale.set(posicaoLida) #VERSAO FINAAAAL
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1


def qualKnob10(event):
    qualKnob = 10
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    #scale.set(posicaoLida.split("\n")[:-1][0])#mostra, no fader, posicao atual do kno
    posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    scale.set(posicaoLida) #VERSAO FINAAAAL
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1

def qualKnob11(event):
    qualKnob = 11
    event.widget.config(borderwidth = 0.8)
    for x in range(1, 12):
        if x != qualKnob:
            globals()['knob%s' % x].config(borderwidth = 0)
    file1 = open("infoKnobs.txt", "w")
    file1.write(str(qualKnob))
    file1.close()
    enviaLer(qualKnob)
    posicaoLida = recebeSerial2()
    #scale.set(posicaoLida.split("\n")[:-1][0])#mostra, no fader, posicao atual do kno
    posicaoLida = float(posicaoLida.split("\n")[:-1][0])/1.8 #VERSAAAO FINAAAL
    scale.set(posicaoLida) #VERSAO FINAAAAL
    posicao = scale.get() #pega valor do scale
    y = 0
    z = 0
    for x in range(1,21):
        z = y + 4
        knobImage = globals()['knobImage%s' %x]
        if y <= posicao <= z: #avalia valor do scale, muda imagem do knob de acordo com o range
            event.widget.config(image = knobImage)
        y = z + 1

def juntaFunc(event):
    enviaMover()
    mudaImagemKnob()


#função p/ enviar mensagem para arduino mover o motor
def enviaMover():
    posicao = scale.get() #lê valor fader/scale
    posicao = int(posicao*1.8) #converte da escala de 0-100 para 0-180 graus
    posicao = str(posicao)
    file1 = open("infoKnobs.txt", "r") #abre arquivo com knob escolhido
    qualKnob = file1.read()
    file1.close()
    qualKnob = str(qualKnob)
    if qualKnob != "0":
        msgMover = str("p" + ord(qualKnob) + "s" + ord(posicao)) #mensagem
        ser.write(msgMover.encode()) #envia arduino
        ser.flushInput()
        ser.flushOutput()
        print(msgMover) #DEPOIS TIRAR
    #print(msgMover) #ESTA AQUI PARA TESTE  DE CONEXAO ARDUINO
'''
def enviaMover1():
    posicao = scale.get() #lê valor fader/scale
    posicao = int(posicao*1.8) #converte da escala de 0-100 para 0-180 graus
    posicao = str(posicao)
    file1 = open("infoKnobs.txt", "r") #abre arquivo com knob escolhido
    qualKnob = file1.read()
    file1.close()
    qualKnob = str(qualKnob)
    if qualKnob != "0":
        msgMover = str(qualKnob + "." + posicao + ".") #mensagem knob.posicao
        ser.write(msgMover.encode()) #envia arduino
        ser.flushInput()
        ser.flushOutput()
        #print(msgMover) #DEPOIS TIRAR
    print(msgMover) #ESTA AQUI PARA TESTE  DE CONEXAO ARDUINO
'''
#def enviaMoverScale(event):
#    root.after(1000, enviaMover1)

#envia comando para arduino p/ ler posicão de determinado knob (motor)
def enviaLer(qualKnob):
    qualKnob = qualKnob + 50
    #print(qualKnob)
    if qualKnob  > 50: #se algum knob foi escolhido
        qualKnob = str(qualKnob)
        msgMover = ("p" + ord(qualKnob)) #mensagem knob.
        ser.write(msgMover.encode()) #envia arduino
        ser.flushInput()
        ser.flushOutput()
        print(msgMover)


#retorna mensagem recebida do arduino
def recebeSerial2():
    msgRecebida1 = ser.readline().decode('ascii')
    msgRecebida2 = ser.readline().decode('ascii')
    ser.flushInput()
    ser.flushOutput()
    mensagem = (msgRecebida1 + msgRecebida2)
    return mensagem

#retorna mensagem recebida do arduino
def recebeSerial1():
    msgRecebida = ser.readline().decode('ascii')
    ser.flushInput()
    ser.flushOutput()
    #print(msgRecebida) #PARA TESTE
    #return msgRecebida


    #envia msg arduino p ler posicoes e armazena
def guardaPreset(nomePreset):
    filePreset = open('%s.txt' %nomePreset, 'a')
    msgRecebida = []
    for x in range(1, 12): #envia comando ler para todos motores
        root.after(10, None) #dar um tempinho para enviar nova msg
        enviaLer(x)
        msg = recebeSerial2()
        filePreset.write(str(x) + "\n")
        filePreset.write(msg) #escreve no arquivo do preset o knov e a posicao
        print(msg)
    filePreset.close()

#verfica se o botao de preset ainda não foi usado, muda o texto dele e guarda preset
def botaoSalva(event):
    file2 = open("listaPresets.txt", "a+")
    numLinhas = sum(1 for line in open("listaPresets.txt"))
    #print(numLinhas)
    for x in range(1, 13):
        nomePreset = globals()['nomePreset%s' % x]
        nome = nomePreset.cget("text")
        presetVar = presetEntry.get()
        #textLabel = nomePreset.cget("text")
        #avisa se já tem um preset salvo com esse nome
        if x <= numLinhas:
            file1 = open("listaPresets.txt", "r") #ler toda a lista
            lines = file1.readlines()
            if os.path.isfile("%s.txt" %presetVar): #acessar linha porlinha
                #print("2")
                #print(lines[x-1].strip())
                #mostrar label avisando temporariamente
                jatemLabel.config(fg = "white", bg = "#231f20")
                root.after(2000, hideLabel)
                break
        #if x > numLinhas:
        if nome == "default" and presetVar != "" and presetVar != " ":
            nomePreset.config(text = str(presetVar))
            file2.write(presetVar + "\n")
            print(presetVar)
            guardaPreset(presetVar)
            break
    file2.close()

#função para aparecer temporariamente label avisando que ja tem preset salvo
def hideLabel():
    jatemLabel.config(fg = "#2b2727", bg = "#2b2727")


#procura se existe o arquivo de listas de presets, atualiza seus nomes nos botoões do banco de presets
def setNomePresets():
        if os.path.isfile("listaPresets.txt"):
            file2 = open("listaPresets.txt", "a+")
            numLinhas = sum(1 for line in open("listaPresets.txt"))
            #lines = linecache.getline("/Users/fegvilela/Documents/RoadieBot/interfacePrototipo1/listaPresets.txt", 1)
            #print(lines)
            for x in range(1, 13):
                if x <= numLinhas:
                    nomePreset = globals()['nomePreset%s' % x]
                    lines = linecache.getline("listaPresets.txt", x) #ler linha por linha
                    nomePreset.config(text = lines[:-1])

#apaga preset selecionado (juntamente com seu arquivo)
def apagarPreset(event):
    for x in range(1, 13):
        nomePreset = globals()['nomePreset%s' % x]
        presetLabel = globals()['presetLabel%s' % x]
        cor = nomePreset.cget("bg")
        if cor == "#dce0df":
            nome = nomePreset.cget("text")
            file1 = open("listaPresets.txt", "r")
            lines = file1.readlines()
            file1.close()
            file1 = open("listaPresets.txt", "w")
            for line in lines:
                if line != ("%s" %nome + "\n"):
                    file1.write(line)
            file1.close()
            os.remove("%s.txt" %nome) #remove arquivo de preset do diretorio
            nomePreset.config(text = "default", fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)


#carregar presets (ler arquivo daquele preset, enviar mensagem p/ arduino mover todos motores, checar qual knob está selecionado - lendo arquivo - e mostrar no fader a sua posicao) - posteriormente, mudar todas as figuras dos knobs para a posicao correta
def loadPreset1(event):
    textLabel = nomePreset1.cget("text") #nome escrito no botão de preset
    print(textLabel)
    #coloca imagem cinza em todos os demais botões
    for x in range(2,13):
        nomePreset = globals()['nomePreset%s' % x]
        presetLabel = globals()['presetLabel%s' % x]
        nomePreset.config(fg = "white", bg = "#231f20")
        presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel1.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset1.config(fg = "#231f20", bg = "#dce0df")
        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1


def loadPreset2(event):
    textLabel = nomePreset2.cget("text") #nome escrito no botão de preset
    print(textLabel + str(2))
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 2:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel2.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset2.config(fg = "#231f20", bg = "#dce0df")
        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset3(event):
    textLabel = nomePreset3.cget("text") #nome escrito no botão de preset
    print(textLabel + str(3))
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 3:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel3.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset3.config(fg = "#231f20", bg = "#dce0df")
        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset4(event):
    textLabel = nomePreset4.cget("text") #nome escrito no botão de preset
    print(textLabel + str(4))
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 4:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel4.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset4.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset5(event):
    textLabel = nomePreset5.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 5:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel5.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset5.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset6(event):
    textLabel = nomePreset6.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 6:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel6.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset6.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset7(event):
    textLabel = nomePreset7.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 7:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel7.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset7.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset8(event):
    textLabel = nomePreset8.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 8:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel8.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset8.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset9(event):
    textLabel = nomePreset9.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 9:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel9.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset9.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset10(event):
    textLabel = nomePreset10.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 10:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel10.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset10.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset11(event):
    textLabel = nomePreset11.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 11:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel11.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset11.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            print(msgMover)
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

def loadPreset12(event):
    textLabel = nomePreset12.cget("text") #nome escrito no botão de preset
    #coloca imagem cinza em todos os demais botões
    for x in range(1,12):
        if x != 12:
            nomePreset = globals()['nomePreset%s' % x]
            presetLabel = globals()['presetLabel%s' % x]
            nomePreset.config(fg = "white", bg = "#231f20")
            presetLabel.config(image = presetImage)
    #verifica se existe o arquivo daquele preset
    if os.path.isfile("%s.txt" %textLabel):
        filePreset = open(("%s.txt" %textLabel), "r")
        lines = filePreset.readlines() #lê todas as linhas do arquivo
        fileKnob = open(("infoKnobs.txt"), "r") #abre arquivo para ver qual knob foi selecionado
        presetLabel12.config(image = presetImageOn) #muda imagem do botão do preset = botão selecionado
        nomePreset12.config(fg = "#231f20", bg = "#dce0df")

        aux = 1
        a = 0
        while(a<11):
        #for a in range(0, 11):
            root.after(50, None) #pequeno delay
            #print(lines[0])
            #lê qual knob no arquivo do preset
            qualKnob = abs(float(lines[2*a].strip()))
            qualKnob = int(qualKnob)
            #lê qual posição no arquivo do preset
            posicao = abs(float(lines[aux].strip()))
            #print(posicao)
            aux = aux + 2
            #mensagem knob.posicao
            msgMover = ("c" + str(qualKnob) + "a" + str(posicao))
            ser.write(msgMover.encode()) #envia arduino
            #limpa conexão seria
            ser.flushInput()
            ser.flushOutput()
            #se algum knob tiver selecionado, muda o valor dele
            x1 = a + 1
            qualKnob2 = fileKnob.readline()
            posicao = posicao/1.8 #passa posicao p/ 0 - 100
            #qualKnob2 = int(qualKnob2.strip())
            if qualKnob2 == x1:
                scale.set(float(posicao))
            a = a + 1
            y = 0
            z = 0
            #muda a imagem de todos knobs
            i = 1
            #root.after(50, None)
            while(i<21):
               #print("i: " + str(i))
            #for i in range(1,21):
                z = y + 4
                knobImage = globals()['knobImage%s' %i]
                posicao = int(posicao)
                if y <= posicao <= z: #avalia valor da posicao, muda imagem do knob de acordo com o range
                    globals()['knob%s' %qualKnob].config(image = knobImage)
                y = z + 1
                i = i + 1

#"sai dos presets" = apaga todos botões e mostra que cliqueou no "off"
def sairBancoPreset(event):
    sairPreset.config(image = motorImageOff1)
    root.after(100, hideLabel2)
    for x in range(1,13):
        nomePreset = globals()['nomePreset%s' % x]
        presetLabel = globals()['presetLabel%s' % x]
        nomePreset.config(fg = "white", bg = "#231f20")
        presetLabel.config(image = presetImage)

#volta pra off cinza
def hideLabel2():
    sairPreset.config(image = motorImageOff2)


#Enviar msg para calibrar motor (e muda a imagem do botão temporariamente) - usar bind
def enviaCalibrar(event):
    event.widget.config(image = calibrarPhotoOn)
    msg = "pc"
    ser.write(msg.encode()) #envia arduino
    msgRecebida = recebeSerial2()
    #print(msgRecebida) #PARA TESTE
    #if msgRecebida.find('99') != -1:
    root.after(1500, mudaBotaoCalibrar) #espera 1s para trocar o botão

#muda o botão de calibrar, depois de  calibrar
def mudaBotaoCalibrar():
    calibrarLabel.config(image = calibrarPhotoOff)

#aciona os servos (attach)
def motorOn(event):
    event.widget.config(image = motorImageOn1)
    motorOffLabel.config(image = motorImageOff2)
    msg = 95
    msg = ("c" + str(msg))
    ser.write(msg.encode()) #envia arduino
    #PARA TESTE
    msgRecebida = recebeSerial2()
   # print(msgRecebida)

#desliga os servos (detach)
def motorOff(event):
    event.widget.config(image = motorImageOff1)
    motorOnLabel.config(image = motorImageOn2)
    msg = 96
    msg = ("c" + str(msg))
    ser.write(msg.encode()) #envia arduino
    #PARA TESTE
    msgRecebida = recebeSerial2()
   # print(msgRecebida)

#Abir janela em fullscreen (this piece's not mine)
class FullScreen(object):
    def __init__(self, master, **kwargs):
        self.master = master
        pad = 3
        self._geom = '200x200+0+0'
        master.geometry("{0}x{1}+0+0".format(
            master.winfo_screenwidth() - pad, master.winfo_screenheight() - pad))
        master.bind('<Escape>',self.toggle_geom)

    def toggle_geom(self,event):
        geom = self.master.winfo_geometry()
        print(geom,self._geom)
        self.master.geometry(self._geom)
        self._geom = geom


#abrir gui fullscreen
root = Tk()
app = FullScreen(root)
root.title("RoadieBot")
root.configure(background = "#2b2727") #color
#inicio = inicio()

root.after(100, inicio)

#imagenm roadiebot
roadieImage = Image.open('iconesInterface1/gifs/icones-07.gif')
roadieImage = roadieImage.resize((267, 75))
roadieImage = ImageTk.PhotoImage(roadieImage)

#label p/ logo
roadieLabel = Label(root, image = roadieImage, borderwidth = 0, highlightthickness = 0)

#place on root
roadieLabel.place(relx = 0.4, rely = 0.03)

#1st rectangle
canvas1 = Canvas(root, bg = "#2b2727",width= 1190, height = 300, borderwidth = 0.5)
canvas1.place(x = 40, y = 120)

#imagens/tamanhos dos knobs
for x in range(1,21):
    globals()['knobImage%s' % x]= Image.open('iconesInterface1/gifs/Botao%s.gif' %x)
    knobSize = 75
    globals()['knobImage%s' % x] = globals()['knobImage%s' % x].resize((knobSize, knobSize))
    globals()['knobImage%s' % x] = ImageTk.PhotoImage(globals()['knobImage%s' % x])

#declaração dos knobs
for x in range(1, 12):
    globals()['knob%s' % x] = Label(canvas1, image = knobImage1, height = knobSize, width = knobSize, borderwidth = 0, highlightthickness = 0)
    #, command = lambda: knobsGui.chooseKnob(self))

#reconhecer clique no knob e selecioná-lo
knob1.bind("<Button-1>", qualKnob1)
knob2.bind("<Button-1>", qualKnob2)
knob3.bind("<Button-1>", qualKnob3)
knob4.bind("<Button-1>", qualKnob4)
knob5.bind("<Button-1>", qualKnob5)
knob6.bind("<Button-1>", qualKnob6)
knob7.bind("<Button-1>", qualKnob7)
knob8.bind("<Button-1>", qualKnob8)
knob9.bind("<Button-1>", qualKnob9)
knob10.bind("<Button-1>", qualKnob10)
knob11.bind("<Button-1>", qualKnob11)

#knob1.bind("<Button-1>", mudaImagemKnob)
#place on gui
knob1.place(x = 18, rely = 0.25)
knob2.place(x = 116, rely = 0.25)
knob3.place(x = 218, rely = 0.25)
knob4.place(x = 310, rely = 0.25)
knob5.place(x = 410, rely = 0.25)
knob6.place(x = 510, rely = 0.25)
knob7.place(x = 610, rely = 0.25)
knob8.place(x = 710, rely = 0.25)
knob9.place(x = 810, rely = 0.25)
knob10.place(x = 910, rely = 0.25)
knob11.place(x = 1010, rely = 0.25)

#knob name labels
knobLabel1 = Label(canvas1, text = "DRIVE", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel2 = Label(canvas1, text = "VOLUME", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel3 = Label(canvas1, text = "TREBLE", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel4 = Label(canvas1, text = "MIDDLE", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel5 = Label(canvas1, text = "BASS", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel6 = Label(canvas1, text = "TREBLE", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel7 = Label(canvas1, text = "MIDDLE", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel8 = Label(canvas1, text = "BASS", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel9 = Label(canvas1, text = "REVERB", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel10 = Label(canvas1, text = "SPEED", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")
knobLabel11 = Label(canvas1, text = "INTENSITY", font = "Helvetica 14 bold", fg = "white", bg = "#231f20")

#place on gui
knobLabel1.place(x = 19, rely = 0.6, anchor = "w")
knobLabel2.place(in_ = knobLabel1, relx = 1.60, rely = -0.11)
knobLabel3.place(in_ = knobLabel2, relx = 1.7, rely = -0.11)
knobLabel4.place(in_ = knobLabel3,relx = 1.75, x = 5, rely = -0.11)
knobLabel5.place(in_ = knobLabel4, relx = 1.85, x = 10, rely = -0.11)
knobLabel6.place(in_ = knobLabel5, relx = 1.7, x = -5, rely = -0.11)
knobLabel7.place(in_ = knobLabel6, relx = 1.75, x = -5, rely = -0.11)
knobLabel8.place(in_ = knobLabel7, relx = 1.75, x = 8, rely = -0.11)
knobLabel9.place(in_ = knobLabel8, relx = 1.95, x = 10, rely = -0.11)
knobLabel10.place(in_ = knobLabel9, relx = 1.75, x = -5, rely = -0.11)
knobLabel11.place(in_ = knobLabel10, relx = 1.75, x = -3, rely = -0.11)

#knob number labels
for x in range(1, 12):
    globals()['knobNum%s' % x] = Label(canvas1, text = str(x), font = "Helvetica 22 bold", fg = "white", bg = "#231f20")

#place on gui
knobNum1.place(x = 45, rely = 0.67)
knobNum2.place(x = 143, rely = 0.67)
knobNum3.place(x = 243, rely = 0.67)
knobNum4.place(x = 343, rely = 0.67)
knobNum5.place(x = 443, rely = 0.67)
knobNum6.place(x = 543, rely = 0.67)
knobNum7.place(x = 643, rely = 0.67)
knobNum8.place(x = 743, rely = 0.67)
knobNum9.place(x = 843, rely = 0.67)
knobNum10.place(x = 938, rely = 0.67)
knobNum11.place(x = 1038, rely = 0.67)

#scale
scale = Scale(canvas1, from_= 100, to = 0, showvalue = 1, bg = "#2b2727", width = 20, length = 150, highlightcolor = "#231f20", fg = "white", highlightbackground = "#231f20", relief = FLAT, activebackground = "black")
scale.place(x = 1130, rely = 0.25)

#colocar função binded no scale
scale.bind("<Button-1>", juntaFunc)
scale.bind("<ButtonRelease>", juntaFunc)
#scale.bind("<B1-Motion>", mudaImagemKnob)
scale.bind("<B1-Motion>", juntaFunc)
#scale.bind("<Button-1>", enviaMover)
#scale.bind("<ButtonRelease>", enviaMover)

#####CALIBRAR

#imagem e tamanho do botão calibrar
#botão desligado
calibrarImageOff = Image.open('iconesInterface1/gifs/icones-09.gif')
calibrarImageOff = calibrarImageOff.resize((163, 80))
calibrarPhotoOff = ImageTk.PhotoImage(calibrarImageOff)
 #botão ligado
calibrarImageOn = Image.open('iconesInterface1/gifs/icones-08.gif')
calibrarImageOn = calibrarImageOn.resize((163, 80))
calibrarPhotoOn = ImageTk.PhotoImage(calibrarImageOn)
#declaração do botão calibrar
calibrarLabel = Label(root, image = calibrarPhotoOff, borderwidth = 0, highlightthickness = 0)
#place on gui
calibrarLabel.place(x = 753, rely = 0.795)
#reconhecer clique no botão e realizar função
calibrarLabel.bind("<Button-1>", enviaCalibrar)


##### MOTOR ON OFF
#canvas para o motor (borda)
canvas2 = Canvas(root, bg = "#2b2727", width = 163, height = 120, borderwidth = 0.5)
#place on gui
canvas2.place(x = 750, rely = 0.6)
#titulo do motor
motorLabel = Label(canvas2, text = "MOTOR", font = "Helvetica 22 bold", fg = "white", bg = "#2b2727")
#place on canvas
motorLabel.place(relx = 0.245, rely = 0.21)
#imagens on e off
#colorida
motorImageOn1 = Image.open('iconesInterface1/gifs/icones-02.gif')
motorImageOn1 = motorImageOn1.resize((59, 40))
motorImageOn1 = ImageTk.PhotoImage(motorImageOn1)
#cinza
motorImageOn2 = Image.open('iconesInterface1/gifs/icones-03.gif')
motorImageOn2 = motorImageOn2.resize((59, 40))
motorImageOn2 = ImageTk.PhotoImage(motorImageOn2)
#colorida
motorImageOff1 = Image.open('iconesInterface1/gifs/icones-04.gif')
motorImageOff1 = motorImageOff1.resize((59, 40))
motorImageOff1 = ImageTk.PhotoImage(motorImageOff1)
#cinza
motorImageOff2 = Image.open('iconesInterface1/gifs/icones-05.gif')
motorImageOff2 = motorImageOff2.resize((59, 40))
motorImageOff2 = ImageTk.PhotoImage(motorImageOff2)

#cria label "on" e place on canvas
motorOnLabel = Label(canvas2, image = motorImageOn1, borderwidth = 0, highlightthickness = 0)
motorOnLabel.place(relx = 0.14, rely = 0.49)

#cria label "off" e place on canvas
motorOffLabel = Label(canvas2, image = motorImageOff2, borderwidth = 0, highlightthickness = 0)
motorOffLabel.place(relx = 0.52, rely = 0.49)

#reconhecer clique nos botões, mudar suas images e enviar msg arduino
motorOnLabel.bind("<Button-1>", motorOn)
motorOffLabel.bind("<Button-1>", motorOff)


###PRESETS

#canvas para o motor (borda)
canvas3 = Canvas(root, bg = "#2b2727",width= 3.3*204, height = 2.35*120, borderwidth = 0.5)
#place on gui
canvas3.place(x = 40, rely = 0.6)

#preset titulo
presetLabel = Label(canvas3, text = "PRESET", font = "Helvetica 22 bold", fg = "white", bg = "#2b2727")
#place on canvas
presetLabel.place(relx = 0.07, rely = 0.06)

#imagem disquete
salvaImage = Image.open('iconesInterface1/gifs/icones-06.gif')
salvaImage = salvaImage.resize((40, 40))
salvaImage = ImageTk.PhotoImage(salvaImage)
#cria label disquete e place on canvas 3
salvaLabel = Label(canvas3, image = salvaImage, borderwidth = 0, highlightthickness = 0)
salvaLabel.place(relx = 0.5, rely = 0.15)


#entry nome preset
#presetVar = StringVar() , textvariable = presetVar
presetEntry = Entry(canvas3, width = 30, bd = 0)
presetEntry.place(relx = 0.08, rely = 0.18)
presetEntry.insert(0, "Insira nome do preset...")
presetEntry.bind('<FocusIn>', on_entry_click)
presetEntry.bind('<FocusOut>', on_focusout)
presetEntry.config(fg = 'grey')

salvaLabel.bind("<Button-1>", botaoSalva)
#salvaLabel.bind("<Button-1>", lambda event: botaoSalva(event, presetVar = presetVar))

#label titulo banco de presets
bancoLabel = Label(canvas3, text = "BANCO DE PRESETS", font = "Helvetica 22 bold", fg = "white", bg = "#2b2727")
#place on canvas
bancoLabel.place(relx = 0.07, rely = 0.29)

#label botão preset sem selecionar
presetImage = Image.open('iconesInterface1/gifs/icones-14.gif')
presetImage = presetImage.resize((135, 60))
presetImage = ImageTk.PhotoImage(presetImage)

#label botão preset selecionado
presetImageOn = Image.open('iconesInterface1/gifs/icones-13.gif')
presetImageOn = presetImageOn.resize((135, 60))
presetImageOn = ImageTk.PhotoImage(presetImageOn)

#label botão preset selecionado
lixeiraImage = Image.open('iconesInterface1/gifs/icones-15.gif')
lixeiraImage = lixeiraImage.resize((37, 37))
lixeiraImage = ImageTk.PhotoImage(lixeiraImage)

#cria label disquete e place on canvas 3
for x in range(1, 13):
    globals()['presetLabel%s' % x] = Label(canvas3, image = presetImage, borderwidth = 0, highlightthickness = 0)
    globals()['nomePreset%s' % x] = Label(canvas3, text = "default", font = "Helvetica 13", fg = "white", bg = "#231f20", borderwidth = 0, highlightthickness = 0, anchor = "center", width = 13)


#label para avisar que ja tem preset salvo com esse nome
jatemLabel = Label(canvas3, text = "Nome repetido!", font = "Helvetica 15 bold", fg = "#2b2727", bg = "#2b2727", borderwidth = 0.2, highlightthickness = 0.2, anchor = "center", width = 13)
#place on canvas
jatemLabel.place(relx = 0.68, rely = 0.19)

#label p sair dos presets
sairPreset = Label(canvas3, image = motorImageOff2, borderwidth = 0, highlightthickness = 0)
#place on canvas
sairPreset.place(relx = 0.875, rely = 0.82)
#perceber clique no botao e chamar função
sairPreset.bind("<Button-1>", sairBancoPreset)

#label para apagar preset
apagaPreset = Label(canvas3, image = lixeiraImage, borderwidth = 0, highlightthickness = 0, bg = "#2b2727" )
#place on canvas
apagaPreset.place(relx = 0.57, rely = 0.15)
#perceber clique no botao e chamar função
apagaPreset.bind("<Button-1>", apagarPreset)

#place on canvas
presetLabel1.place(relx = 0.06, rely = 0.38)
presetLabel2.place(relx = 0.06, rely = 0.58)
presetLabel3.place(relx = 0.06, rely = 0.78)
presetLabel4.place(relx = 0.06 + 0.2, rely = 0.38)
presetLabel5.place(relx = 0.06 + 0.2, rely = 0.58)
presetLabel6.place(relx = 0.06 + 0.2, rely = 0.78)
presetLabel7.place(relx = 0.06 + 0.4, rely = 0.38)
presetLabel8.place(relx = 0.06 + 0.4, rely = 0.58)
presetLabel9.place(relx = 0.06 + 0.4, rely = 0.78)
presetLabel10.place(relx = 0.06 + 0.6, rely = 0.38)
presetLabel11.place(relx = 0.06 + 0.6, rely = 0.58)
presetLabel12.place(relx = 0.06 + 0.6, rely = 0.78)

#place on canvas
nomePreset1.place(relx = 0.085, rely = 0.45)
nomePreset2.place(relx = 0.085, rely = 0.65)
nomePreset3.place(relx = 0.085, rely = 0.85)
nomePreset4.place(relx = 0.085 + 0.2, rely = 0.45)
nomePreset5.place(relx = 0.085 + 0.2, rely = 0.65)
nomePreset6.place(relx = 0.085 + 0.2, rely = 0.85)
nomePreset7.place(relx = 0.085 + 0.4, rely = 0.45)
nomePreset8.place(relx = 0.085 + 0.4, rely = 0.65)
nomePreset9.place(relx = 0.085 + 0.4, rely = 0.85)
nomePreset10.place(relx = 0.085 + 0.6, rely = 0.45)
nomePreset11.place(relx = 0.085 + 0.6, rely = 0.65)
nomePreset12.place(relx = 0.085 + 0.6, rely = 0.85)


#perceber clique no botao e chamar função
presetLabel1.bind("<Button-1>", loadPreset1)
presetLabel2.bind("<Button-1>", loadPreset2)
presetLabel3.bind("<Button-1>", loadPreset3)
presetLabel4.bind("<Button-1>", loadPreset4)
presetLabel5.bind("<Button-1>", loadPreset5)
presetLabel6.bind("<Button-1>", loadPreset6)
presetLabel7.bind("<Button-1>", loadPreset7)
presetLabel8.bind("<Button-1>", loadPreset8)
presetLabel9.bind("<Button-1>", loadPreset9)
presetLabel10.bind("<Button-1>", loadPreset10)
presetLabel11.bind("<Button-1>", loadPreset11)
presetLabel12.bind("<Button-1>", loadPreset12)

#perceber clique no botao e chamar função
nomePreset1.bind("<Button-1>", loadPreset1)
nomePreset2.bind("<Button-1>", loadPreset2)
nomePreset3.bind("<Button-1>", loadPreset3)
nomePreset4.bind("<Button-1>", loadPreset4)
nomePreset5.bind("<Button-1>", loadPreset5)
nomePreset6.bind("<Button-1>", loadPreset6)
nomePreset7.bind("<Button-1>", loadPreset7)
nomePreset8.bind("<Button-1>", loadPreset8)
nomePreset9.bind("<Button-1>", loadPreset9)
nomePreset10.bind("<Button-1>", loadPreset10)
nomePreset11.bind("<Button-1>", loadPreset11)
nomePreset12.bind("<Button-1>", loadPreset12)


root.mainloop()
